package com.example.projet;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class ProductActivity extends AppCompatActivity {


    public MuseumProduct m ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        // m = (MuseumProduct) getIntent().getParcelableExtra("produit");

         // on recupere le Wine selectionner sur la listView
        Bundle extras = getIntent().getExtras();
        m = (MuseumProduct) extras.get("produit");






        TextView name1 = (TextView) findViewById(R.id.name1);

        TextView description1 = (TextView) findViewById(R.id.description1);
        TextView timeFrame1 = (TextView) findViewById(R.id.timeFrame1);
        TextView year1 = (TextView) findViewById(R.id.year1);
        TextView brand1 = (TextView) findViewById(R.id.brand1);
        TextView technicalDetails1 = (TextView) findViewById(R.id.technicalDetails1);
        TextView workingText = (TextView) findViewById(R.id.workingText);


        name1.setText(m.getName());

        description1.setText(m.getDescription());
        timeFrame1.setText(m.getTimeFrame());
        //si on a pas de date
        if(m.getYear().equals("0")){
            year1.setText("Non renseigné");
        }else{
            year1.setText(m.getYear());
        }

        brand1.setText(m.getBrand());
        technicalDetails1.setText(m.getTechnicalDetails());
        workingText.setText(m.getThumbnail());




    }
}
