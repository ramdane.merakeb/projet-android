package com.example.projet;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.service.autofill.FieldClassification;

import androidx.annotation.RequiresApi;

import java.util.HashMap;
import java.util.List;

public class MuseumProduct implements Parcelable {

    public static final String TAG = MuseumProduct.class.getSimpleName();

    private long id; // used for the _id colum of the db helper

    private String name;
    private String idProduct; // used for web service

    private String categories;
    //private List<String> categories;

    private String description;

    private String timeFrame;
    //private List<String> timeFrame;

    private int year;
    private String brand;

    private String technicalDetails;
    //private List<String> technicalDetails;

    private String working;
    private String thumbnail;


    //private HashMap<String, String> pictures ;
    //private String pictures;

    public MuseumProduct(String idProduct) {
        this.idProduct = idProduct;
    }



    public MuseumProduct(long id,  String name,String idProduct, String categories, String description, String timeFrame,int year, String brand, String technicalDetails, String working, String thumbnail) {
        this.id = id;
        this.idProduct = idProduct;
        this.name = name;
        this.categories = categories;
        this.description = description;
        this.timeFrame = timeFrame;
        this.year = year;
        this.brand = brand;
        this.technicalDetails = technicalDetails;
        this.working = working;
        this.thumbnail = thumbnail;

    }

    public long getId() {
        return id;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public String getName() {
        return name;
    }

    public String getCategories() {
        return categories;
    }


    public String getDescription() {
        return description;
    }

    public String getTimeFrame() { return timeFrame; }



    public String getYear() { return Integer.toString(year);}

    public String getBrand() {
        return brand;
    }

    public String getTechnicalDetails() {
        return technicalDetails;
    }

    public String getWorking() {
        return working;
    }

    public String getThumbnail() {
        return thumbnail;
    }



    public void setId(long id) { this.id = id;}

    public void setName(String name) {
        this.name = name;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public void setDescription(String description) { this.description = description; }



    public void setTimeFrame(String timeFrame) {
        this.timeFrame = timeFrame;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setTechnicalDetails(String technicalDetails) {
        this.technicalDetails = technicalDetails;

    }

    public void setWorking(String working) {
        this.working = working;

    }
    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;

    }

    public boolean searchValue(String query){
        query = query.toLowerCase();
        if(technicalDetails.toLowerCase().replace('é','e').replace('è','e').replace('ô','o').replace('ê','e').contains(query)) return true;
        if(Integer.toString(year).toLowerCase().replace('é','e').replace('è','e').replace('ô','o').replace('ê','e').contains(query)) return true;
        if(name.toLowerCase().replace('é','e').replace('è','e').replace('ô','o').replace('ê','e').contains(query)) return true;
        if(idProduct.toLowerCase().replace('é','e').replace('è','e').replace('ô','o').replace('ê','e').contains(query)) return true;
        if(description.toLowerCase().replace('é','e').replace('è','e').replace('ô','o').replace('ê','e').contains(query)) return true;
        if(brand.toLowerCase().replace('é','e').replace('è','e').replace('ô','o').replace('ê','e').contains(query)) return true;
        if(categories.toLowerCase().replace('é','e').replace('è','e').replace('ô','o').replace('ê','e').contains(query)) return true;
        if(timeFrame.toLowerCase().replace('é','e').replace('è','e').replace('ô','o').replace('ê','e').contains(query)) return true;

        return false;
    }


    /*private void setLastUpdate() {
        Date currentTime = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss");
        this.lastUpdate = dateFormat.format(currentTime);
    }

    @Override
    public String toString() {
        return this.name+"("+this.league +"): "+this.lastEvent +"°C";
    }*/

    @Override
    public int describeContents() {
        return 0;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(idProduct);
        dest.writeString(categories);
        dest.writeString(description);
        dest.writeString(timeFrame);
        dest.writeInt(year);
        dest.writeString(brand);
        dest.writeString(technicalDetails);
        dest.writeString(thumbnail);

        dest.writeString(working);
    }


    public static final Creator<MuseumProduct> CREATOR = new Creator<MuseumProduct>()
    {

        @Override
        public MuseumProduct createFromParcel(Parcel source)
        {
            return new MuseumProduct(source);
        }

        @Override
        public MuseumProduct[] newArray(int size)
        {
            return new MuseumProduct[size];
        }
    };


    public MuseumProduct(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.idProduct = in.readString();
        this.categories = in.readString();
        this.description = in.readString();
        this.timeFrame = in.readString();
        this.year = in.readInt();
        this.brand = in.readString();
        this.technicalDetails = in.readString();
        this.working = in.readString();
        this.thumbnail = in.readString();
    }

}
