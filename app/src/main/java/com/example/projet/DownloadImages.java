package com.example.projet;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.net.URL;

public class DownloadImages extends AsyncTask<ImageView, Void, Bitmap> {

    ImageView imageView;
    MuseumProduct product;

    public DownloadImages(ImageView imageView, MuseumProduct product) {
        this.imageView = imageView;
        this.product = product;
    }

    @Override
    protected Bitmap doInBackground(ImageView... imageViews) {
        Bitmap bmp = null;

        try {
            URL url = new URL(this.product.getThumbnail());
            bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (Exception e) {
            Log.d("error_badge",e.toString());
        }

        return bmp;

    }
    @Override
    protected void onPostExecute(Bitmap result) {

        imageView.setImageBitmap(result);

    }

}
