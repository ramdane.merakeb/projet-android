package com.example.projet;

import android.net.Uri;

import java.net.MalformedURLException;
import java.net.URL;

public class WebServiceUrl {



    private static final String HOST = "demo-lia.univ-avignon.fr";
    private static final String PATH_1 = "cerimuseum";


    private static Uri.Builder commonBuilder() {
        Uri.Builder builder = new Uri.Builder();

        builder.scheme("https")
                .authority(HOST)
                .appendPath(PATH_1);
        return builder;
    }

    // Get information ids
    // https://demo-lia.univ-avignon.fr/cerimuseum/ids
    private static final String IDS = "ids";


    public static URL buildSearchIds() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(IDS);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get CATEGORIES
    // https://demo-lia.univ-avignon.fr/cerimuseum/categories
    private static final String CATEGORIES = "categories";

    public static URL buildSearchCategories( ) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(CATEGORIES);
        URL url = new URL(builder.build().toString());
        return url;
    }


    private static final String CATALOGUE = "catalog";

    public static URL buildSearchcatalog() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(CATALOGUE);
        URL url = new URL(builder.build().toString());
        return url;
    }

    private static final String ITEMS = "items";

    public static URL buildSearchItems(String itemId) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(ITEMS)
                .appendPath(itemId)   ;
        URL url = new URL(builder.build().toString());
        return url;
    }

    private static final String   THUMBNAIL = "thumbnail" ;

    public static URL buildSearchThumbnail(String itemId) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(ITEMS)
                .appendPath(itemId)
                .appendPath(THUMBNAIL);

        URL url = new URL(builder.build().toString());
        return url;
    }

    private static final String   IAMGES = "images" ;

    public static URL buildSearchThumbnail(String itemId, String imgId) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(ITEMS)
                .appendPath(itemId)
                .appendPath(IAMGES)
                .appendPath(imgId);

        URL url = new URL(builder.build().toString());
        return url;
    }

    private static final String  DEMOS = "demos" ;

    public static URL buildSearchDemos() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(DEMOS);

        URL url = new URL(builder.build().toString());
        return url;
    }



}