package com.example.projet;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    private Context context;
    private List<MuseumProduct> list ;

    public ProductAdapter(Context context , List<MuseumProduct> list) {

        this.context = context;
        this.list = list;
    }



    // to inflate the item layout and create the holder
    @Override
    public ViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        LayoutInflater myInflater = LayoutInflater.from(context);
        View view = myInflater.inflate(R.layout.product_items,parent,false);
        return new ViewHolder(view);

    }

    // Involves populating data into the item through holder
    //to set the view attributes based on the data
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if (!list.isEmpty()){
            final MuseumProduct product = list.get(position);

            new DownloadImages(holder.productImg, product).execute();
            holder.productName.setText(product.getName());
            holder.productCat.setText(product.getCategories());
            holder.productBrand.setText(product.getBrand());


            holder.productLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent myIntent = new Intent(context, ProductActivity.class);
                    myIntent.putExtra("produit", list.get(position));
                    context.startActivity(myIntent);
                }
            });
        }else{
            final MuseumProduct product = CatalogueActivity.DB.getAllProduct().get(position);
            new DownloadImages(holder.productImg, product).execute();
            holder.productName.setText(product.getName());
            holder.productCat.setText(product.getCategories());
            holder.productBrand.setText(product.getBrand());


            holder.productLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent myIntent = new Intent(context, ProductActivity.class);
                    myIntent.putExtra("produit", CatalogueActivity.DB.getAllProduct().get(position));
                    context.startActivity(myIntent);
                }
            });
        }




    }

    @Override
    public int getItemCount() {
        if(!list.isEmpty()){
            return list.size();
        }else{
            return CatalogueActivity.DB.getAllProduct().size();
        }
    }

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public ImageView productImg;
        public TextView productName;
        public TextView productCat;
        public TextView productBrand;
        public ConstraintLayout productLayout;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            productImg = (ImageView)  itemView.findViewById(R.id.productImg);
            productName = (TextView) itemView.findViewById(R.id.productName);
            productCat = (TextView) itemView.findViewById(R.id.productCat);
            productLayout = (ConstraintLayout) itemView.findViewById(R.id.product_layout);
            productBrand = (TextView) itemView.findViewById(R.id.productBrand);
        }
    }


}
