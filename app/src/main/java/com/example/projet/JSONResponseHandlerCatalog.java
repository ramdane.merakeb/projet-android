package com.example.projet;

import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.example.projet.MuseumProduct;

public class JSONResponseHandlerCatalog {

    private static final String TAG = JSONResponseHandlerCatalog.class.getSimpleName();

    private MuseumDbHelper dbProduct;



    public JSONResponseHandlerCatalog(MuseumDbHelper dbProduct) {
        this.dbProduct = dbProduct;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStreamCatalog(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readCatalog(reader);
        } finally {
            reader.close();
        }
    }

    public void readCatalog(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String id = reader.nextName();
            //on recupere l'id et on construit l'objet
            MuseumProduct product = new MuseumProduct(id);
            readArrayCatalog(reader,product);
        }
        reader.endObject();
    }


    private void readArrayCatalog(JsonReader reader, MuseumProduct product) throws IOException {
        reader.beginObject();

        while (reader.hasNext() ) {
            String name = reader.nextName();
            //recuperation des information du produit
            if (name.equals("name")) {
                product.setName(reader.nextString());
            }else if (name.equals("brand")){
                product.setBrand(reader.nextString());
            }else if (name.equals("year")){
                product.setYear(reader.nextInt());
            }else if (name.equals("description")){
                product.setDescription(reader.nextString());
            }else if (name.equals("working")){
                product.setWorking("fonctionne");
                reader.nextBoolean();

                //product.setWorking(Boolean.toString(reader.nextBoolean()));
               /* boolean b = reader.nextBoolean();
                if(b){
                    product.setWorking("fonctionne");
                }else{
                    product.setWorking("fonctionne pas");
                }*/
            }else if (name.equals("timeFrame")){
                reader.beginArray();
                //je stock les dates dans un string et je seppare les dates avec une virgule
                StringBuilder s = new StringBuilder();
                while (reader.hasNext()){
                    s.append(reader.nextInt());
                    if(reader.hasNext()){
                        s.append(",");
                    }
                }
                product.setTimeFrame(s.toString());
                reader.endArray();
            }else if (name.equals("categories")){
                reader.beginArray();
                //je stock les categories dans un string et je seppare les categories avec une virgule
                StringBuilder s = new StringBuilder();
                while (reader.hasNext()){
                    s.append(reader.nextString());
                    if(reader.hasNext()){
                        s.append(" , ");
                    }
                }
                product.setCategories(s.toString());
                reader.endArray();
            }else if (name.equals("technicalDetails")){
                reader.beginArray();
                //je stock les Details technique dans un string et je les seppare  avec une virgule
                StringBuilder s = new StringBuilder();
                while (reader.hasNext()){
                    s.append(reader.nextString());
                    if(reader.hasNext()){
                        s.append(" , ");
                    }
                }
                product.setTechnicalDetails(s.toString());
                reader.endArray();
            }else {
                reader.skipValue();
            }



        }
        reader.endObject();

        //verifier les produit qui ne fonctionnes pas
        if( product.getWorking() == null || product.getWorking().isEmpty()){
            product.setWorking("fonctionne pas");
        }

        //remplacer les champs vides par non renseigner pour ameliorer l'affichage
        if(product.getDescription() == null || product.getDescription().isEmpty() ){
            product.setDescription("Non renseigné");
        }
        if(product.getTechnicalDetails() == null || product.getTechnicalDetails().isEmpty()){
            product.setTechnicalDetails("Non renseigné");
        }
        if(product.getBrand() == null || product.getBrand().isEmpty()){
            product.setBrand("Non renseigné");
        }




        //reglages de quelques nom de produit
        if(product.getIdProduct().equals("k0j")){
            product.setName("Apple 2c");
        }else if(product.getIdProduct().equals("jne")){ //c
            product.setName("Monitor 2c");
        }else if(product.getIdProduct().equals("tnx")){
            product.setName("T07 70");
        }


        //insersion des icon
        product.setThumbnail("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+product.getIdProduct()+"/thumbnail");

        //ajout du produit dans la bd
        dbProduct.addProduct(product);


    }
}
