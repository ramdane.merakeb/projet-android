package com.example.projet;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;


public class MuseumDbHelper extends SQLiteOpenHelper {
    private static final String TAG = MuseumDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "museum.db";

    public static final String TABLE_NAME = "product";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "product";
    public static final String COLUMN_PRODUCT_ID = "idProduct";
    public static final String COLUMN_CATEGORIES = "categories";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_TIME_FRAME = "timeFrame";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_BRAND = "brand";
    public static final String COLUMN_TECHNICAL_DETAILS = "technicalDetails";
    public static final String COLUMN_WORKING = "working";
    public static final String COLUMN_THUMBNAIL = "thumbnail";


    public MuseumDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        final String SQL_CREATE_BOOK_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY," +
                COLUMN_NAME + " TEXT NOT NULL, " +
                COLUMN_PRODUCT_ID + " TEXT, " +
                COLUMN_CATEGORIES + " TEXT, " +
                COLUMN_DESCRIPTION + " TEXT, " +
                COLUMN_TIME_FRAME + " TEXT, " +
                COLUMN_YEAR + " INTEGER, " +
                COLUMN_BRAND + " TEXT, " +
                COLUMN_TECHNICAL_DETAILS + " TEXT, " +
                COLUMN_WORKING + " TEXT, " +
                COLUMN_THUMBNAIL + " TEXT, " +

                // To assure the application have just one team entry per
                // product name , it's created a UNIQUE
                " UNIQUE (" + COLUMN_PRODUCT_ID + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_BOOK_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /**
     * Fills ContentValues result from a MuseumProduct object
     */
    private ContentValues fill(MuseumProduct product) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, product.getName());
        values.put(COLUMN_PRODUCT_ID, product.getIdProduct());
        values.put(COLUMN_CATEGORIES, product.getCategories());
        values.put(COLUMN_DESCRIPTION, product.getDescription());
        values.put(COLUMN_TIME_FRAME, product.getTimeFrame());
        values.put(COLUMN_YEAR, product.getYear());
        values.put(COLUMN_BRAND, product.getBrand());
        values.put(COLUMN_TECHNICAL_DETAILS, product.getTechnicalDetails());
        values.put(COLUMN_WORKING, product.getWorking());

        values.put(COLUMN_THUMBNAIL, product.getThumbnail());

        return values;
    }

    /**
     * Adds a new product
     * @return  true if the product was added to the table ; false otherwise (case when team is
     * already in the data base)
     */
    public boolean addProduct(MuseumProduct product) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = fill(product);

        Log.d(TAG, "adding: "+product.getName()+" with id="+product.getId());

        // Inserting Row
        // The unique used for creating table ensures to have only one copy of each team
        // If rowID = -1, an error occured
        long rowID = db.insertWithOnConflict(TABLE_NAME, null, values, CONFLICT_IGNORE);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Updates the information of a MuseumProduct inside the data base
     * @return the number of updated rows
     */
    public int updateProduct(MuseumProduct product) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = fill(product);

        // updating row
        return db.updateWithOnConflict(TABLE_NAME, values, _ID + " = ?",
                new String[] { String.valueOf(product.getId()) }, CONFLICT_IGNORE);
    }

    /**
     * Returns a cursor on all the MuseumProduct of the data base
     */
    public Cursor fetchAllProduct() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null,
                null, null, null, null, COLUMN_NAME +" ASC", null);

        Log.d(TAG, "call fetchAllProduct()");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    /**
     * Returns a list on all the MuseumProduct of the data base
     */
    public List<MuseumProduct> getAllProduct() {

        List<MuseumProduct> res = new ArrayList<>();
        Cursor c = this.fetchAllProduct() ;

        c.moveToFirst();
        while (!c.isAfterLast()){
            MuseumProduct product = this.cursorToProduct(c);
            res.add(product);
            c.moveToNext();
        }
        return res;
    }

    public void deleteTeam(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, _ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();
    }

    /*public void populate() {
        Log.d(TAG, "call populate()");
        addTeam(new Team("RC Toulonnais", "Top 14"));
        addTeam(new Team("ASM Clermont Auvergne", "Top 14"));
        addTeam(new Team("Stade Rochelais", "Top 14"));
        addTeam(new Team("Bath Rugby","Rugby Union Premiership"));
        addTeam(new Team("Edinburgh","Pro14"));
        addTeam(new Team("Stade Toulousain", "Top 14"));
        addTeam(new Team("Wasps","Rugby Union Premiership"));
        addTeam(new Team("Bristol Rugby","ugby Union Premiership"));
        addTeam(new Team("CA Brive","Pro14"));

        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }*/

    public MuseumProduct cursorToProduct(Cursor cursor) {

        MuseumProduct product = new MuseumProduct(cursor.getLong(cursor.getColumnIndex(_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_NAME)),
                cursor.getString(cursor.getColumnIndex(COLUMN_PRODUCT_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORIES)),
                cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)),
                cursor.getString(cursor.getColumnIndex(COLUMN_TIME_FRAME)),
                cursor.getInt(cursor.getColumnIndex(COLUMN_YEAR)),
                cursor.getString(cursor.getColumnIndex(COLUMN_BRAND)),
                cursor.getString(cursor.getColumnIndex(COLUMN_TECHNICAL_DETAILS)),
                cursor.getString(cursor.getColumnIndex(COLUMN_WORKING)),
                cursor.getString(cursor.getColumnIndex(COLUMN_THUMBNAIL))
        );

        return product;
    }

    public MuseumProduct getProduct(int id) {
        MuseumProduct product = null;

        SQLiteDatabase db = this.getReadableDatabase();

        String sql = String.format(
                "SELECT * FROM %s WHERE %s = %d",
                MuseumDbHelper.TABLE_NAME,
                MuseumDbHelper._ID,
                id
        );
        Cursor c= db.rawQuery(sql, null);

        if (c != null) {

            if (c.moveToFirst()) {
                product = this.cursorToProduct(c);
            }
        }

        return  product;  //product;
    }
}
