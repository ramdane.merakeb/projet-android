package com.example.projet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button catalogue , categorie ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        catalogue = (Button) findViewById(R.id.catalogue);
        categorie = (Button) findViewById(R.id.categorie);

        catalogue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentWine = new Intent(view.getContext(), CatalogueActivity.class);
                Log.d("ajout de Wine", "bien ajouté");
                intentWine.putExtra("wineSelected", (Bundle) null);
                startActivity(intentWine);
            }
        });

        categorie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentWine = new Intent(view.getContext(), CategorieActivity.class);
                Log.d("ajout de Wine", "bien ajouté");
                intentWine.putExtra("wineSelected", (Bundle) null);
                startActivity(intentWine);
            }
        });


    }
}
