package com.example.projet;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;


public class CatalogueActivity extends AppCompatActivity {

    RecyclerView rvCatalogue;
    ImageButton searchButton;
    EditText productSearch ;
    public static SwipeRefreshLayout swipeContainer;


    public static MuseumDbHelper dbProduct;
    public static ProductAdapter adapter;

    public static MuseumDbHelper DB;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalogue);

        CatalogueActivity.DB = new MuseumDbHelper(this);

        dbProduct = new MuseumDbHelper(this);
        rvCatalogue = (RecyclerView) findViewById(R.id.rvCatalogue);
        adapter = new ProductAdapter(this, new ArrayList<MuseumProduct>());

        rvCatalogue.setAdapter(adapter);
        rvCatalogue.setLayoutManager(new LinearLayoutManager(this));

        swipeContainer = findViewById(R.id.swipeContainer);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                new AsyncTaskCatalog().execute(dbProduct);
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        Cursor mCursor = dbProduct.getReadableDatabase().rawQuery("SELECT * FROM " + MuseumDbHelper.TABLE_NAME, null);
        boolean asynkCatalog = false;
        if(mCursor.getCount() == 0) {
            new AsyncTaskCatalog().execute(dbProduct);
            asynkCatalog = true;

        }

        searchButton = (ImageButton) findViewById(R.id.searchButton);
        productSearch = (EditText) findViewById(R.id.productSearch);

        searchButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String s = productSearch.getText().toString();

                if(s == null || s.isEmpty()){

                    AlertDialog.Builder builder = new AlertDialog.Builder(CatalogueActivity.this);
                    builder.setTitle("Recherche Impossibe .");
                    builder.setMessage("le champs ne doit pas etre vide !");
                    builder.setCancelable(true);
                    builder.show();

                }else{

                    searchProduct(s);
                }



            }
        });




    }




    public void searchProduct(String r){
        List<MuseumProduct> res = new ArrayList<>();
        //list = dbProduct.getAllProduct();

        for(MuseumProduct product: dbProduct.getAllProduct()){
            if(product.searchValue(r))
                res.add(product);
        }
        if (!res.isEmpty()){
            rvCatalogue.setAdapter(new ProductAdapter(this, res));
        }else{
            AlertDialog.Builder builder = new AlertDialog.Builder(CatalogueActivity.this);
            builder.setTitle("Objet non trouvé.");
            builder.setMessage("corrigez votre recherche");
            builder.setCancelable(true);
            builder.show();
        }

    }

    public class AsyncTaskCatalog extends AsyncTask<MuseumDbHelper , String, Boolean>{

        private ProgressDialog progressDialog;

        private JSONResponseHandlerCatalog JSONCatalog;
        private URL urlCatalog;
        private InputStream isCatalog ;
        HttpsURLConnection urlConnection = null;

        protected void onPreExecute(){
            super.onPreExecute();
            progressDialog = new ProgressDialog(CatalogueActivity.this);
            progressDialog.setMessage("Téléchargement des Produits");
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(MuseumDbHelper... museumDbHelpers) {
            try {
                urlCatalog = WebServiceUrl.buildSearchcatalog();
                urlConnection = (HttpsURLConnection) urlCatalog.openConnection();
                JSONCatalog  = new JSONResponseHandlerCatalog(dbProduct);
                try {
                    isCatalog = new BufferedInputStream(urlConnection.getInputStream());
                    JSONCatalog.readJsonStreamCatalog(isCatalog);
                } finally {
                    urlConnection.disconnect();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }

        protected void onPostExecute(Boolean bool){
            super.onPostExecute(bool);
            adapter.notifyDataSetChanged();
            swipeContainer.setRefreshing(false);
            progressDialog.dismiss();
        }


    }




}
